package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Yoga;

@Repository
public interface YogaRepository extends JpaRepository<Yoga, Integer>{
	
	
}
