package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.Repository;
import org.springframework.stereotype.Service;

import com.model.Customer;
import com.model.Yoga;

@Service
public class YogaDao {
	
	@Autowired
	YogaRepository yogaRepository;

	public List<Yoga> getYogaItems() {
		return yogaRepository.findAll();
	}

	public Yoga addYogaItems(Yoga yoga) {
		Yoga savedYogaItems = yogaRepository.save(yoga);
		return savedYogaItems;
	}
	
}
