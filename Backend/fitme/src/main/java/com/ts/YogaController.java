package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.YogaDao;
import com.model.Yoga;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class YogaController {
	
	@Autowired
	YogaDao yogaDao;
	
	@GetMapping("getYogaItems")
	public List<Yoga> getYogaItems() {
		return yogaDao.getYogaItems();
	}
	
	@PostMapping("addYogaItems")
	public Yoga addYogaItems(@RequestBody Yoga yoga) {
		return yogaDao.addYogaItems(yoga);
	}

}
