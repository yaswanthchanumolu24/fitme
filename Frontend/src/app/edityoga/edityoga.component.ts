import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edityoga',
  templateUrl: './edityoga.component.html',
  styleUrl: './edityoga.component.css'
})

export class EdityogaComponent implements OnInit{

  item : any;

  constructor(private service : ServicesService , private toastr : ToastrService){
    this.item = {
      name : '',
      description : '',
      price : '',
      imageUrl : ''
    };

  }

  ngOnInit(){
    
  }

  submitProduct(productForm: any){
    this.item.name = productForm.name;
    this.item.description = productForm.description;
    this.item.price = productForm.price;
    this.item.imageUrl = productForm.imageUrl;
    console.log(this.item);
    this.service.AddYogaItems(this.item).subscribe((data:any) =>{
      console.log("added item successfully" , data);
      this.toastr.success('Successful added ', 'Success'); 
    });
  }

}
