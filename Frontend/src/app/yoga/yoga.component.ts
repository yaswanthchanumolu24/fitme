import { Component } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-yoga',
  templateUrl: './yoga.component.html',
  styleUrl: './yoga.component.css'
})
export class YogaComponent {
  products: any;
  emailId: any;
  selectedProduct: any;
  cartProducts: any;


  constructor(private service: ServicesService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = []; // Initialize products as an empty array
    this.loadProducts(); // Load products asynchronously
  }

  loadProducts() {
    this.service.getYogaItems().subscribe(
      (data: any) => {
        this.products = data; // Assign the received array to products
        console.log("Yoga items loaded");
      },
      (error) => {
        console.error("Error loading yoga items:", error);
      }
    );
  }

  addToCart(product: any) {
    this.service.addToCart(product);
  }
}